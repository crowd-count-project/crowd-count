# CROWD COUNT
Crowd Counting is a technique to count or estimate the number of people in an image. Take a moment to analyze the an image of people sitting in a stadium

Can you give me an approximate number of how many people are in the frame? 
Yes, including the ones present way in the background.
The most direct method is to manually count each person but does that make practical sense? 
It’s nearly impossible when the crowd is this big!

Crowd scientists (yes, that’s a real job title!) count the number of people in certain parts of an image and then extrapolate to come up with an estimate.
More commonly, we have had to rely on crude metrics to estimate this number for decades.

Surely there must be a better, more exact approach?
Yes, there is!

While we don’t yet have algorithms that can give us the EXACT number, most computer vision techniques can produce impressively precise estimates. Let’s first understand why crowd counting is important before diving into the algorithm behind it.
